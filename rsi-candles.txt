// This source code is subject to the terms of the Mozilla Public License 2.0 at https://mozilla.org/MPL/2.0/
// © mwhitaker08

//@version=4
study(title="RSI Candles", overlay=true)
rsiSource = input(title="RSI Source", type=input.source, defval=close)
rsiLength = input(title="RSI Length", type=input.integer, defval=14)
rsiOverbought = input(title="RSI Overbought Level", type=input.integer, defval=50)
rsi = rsi(rsiSource, rsiLength)

paletteColor = rsi >= 70 ? color.purple : rsi >= 50 ? color.green : rsi < 30 ? color.yellow : rsi < 50 ? color.red : na
barcolor(color=paletteColor)