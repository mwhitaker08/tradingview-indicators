# TradingView Indicators

## Getting started
Copy paste each indicator's code into TradingView's Pine Editor.

- To add them, go down to the bottom of the screen in the browser where it says "Pine Editor". Click on that and hit "open">"new blank indicator".
- Copy paste my code for one of them in. Give it a name and save it. Then hit "add to chart". 
- You'll find indicators normally in the top nav bar which looks like "Fx". "My Scripts" will have all these and you can search for any others.
- You'll need to make a new indicator for each file I provided. 
- Each indicator has a settings cog wheel when you hover over their name. 
- Swiss Army you'll need to turn off/on any of them that you don't want. A lot may show up on the screen at first until you turn them off. Try to keep it simple, don't overcomplicate what you have on screen.
